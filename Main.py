from SetUp import SetUp as su
from Poms import GoogleMain as gm
from Poms import GoogleSearch as gs
import unittest


class GooglePage (unittest.TestCase):
    def setUp(self):
        # Se instancia un webdriver
        setup = su.SetUp()
        # Se le asigna a una variable
        self.chrome = setup.getDriver()
        # Se navega a la página de google
        self.chrome.get("https://www.google.com/")

    def test_search_in_google(self):
        chrome = self.chrome
        # Se instancia una pagina principal
        principal = gm.GoogleMain(chrome)
        # Assert que comprueba el titulo de la pagina
        self.assertIn("Google", chrome.title)
        # Se recibe la busqueda en consola y se realiza
        principal.escribirBusqueda(principal.pedirBusqueda())
        # Se instancia una pagina de busquedas
        searchPage = gs.GoogleSearch(chrome)
        # Se valida si hay sugerencias
        searchPage.validarSugerencia()
        # Se imprime la lista de resultados
        searchPage.imprimeBusqueda()
        # Se valida si todos los resultados tienen descripcion
        result = searchPage.validarDescripcion()
        # Revisa que el resultado sea true
        self.assertEqual(result, True)

    def tearDown(self):
        chrome = self.chrome
        # Se cierra el navegador
        chrome.close()


if __name__ == "__main__":
    unittest.main()
