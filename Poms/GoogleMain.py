from selenium import webdriver
from selenium.webdriver.common.keys import Keys
# Clase POM para la pagina principal de google


class GoogleMain():

    def __init__(self, driver):
        self.driver = driver
        self.searchText = driver.find_element_by_xpath("//input[@name='q']")
        self.serchButton = driver.find_element_by_xpath("//div[@class='FPdoL" +
                                            "c tfB0Bf']//input[@name='btnK']")
    # Se ingresa la busqueda dentro de la barra de busquedas

    def escribirBusqueda(self, palabra):
        self.searchText.send_keys(palabra + Keys.RETURN)
        print("Busqueda escrita")
    # Funcion que da click en el boton de busqueda

    def clickBusqueda(self):
        self.serchButton.click()
        print("Busqueda realizada")
    # Funcion que pide la busqueda a ingresar

    def pedirBusqueda(self):
        busqueda = input("Ingrese la busqueda a realizar")
        return busqueda
