from selenium import webdriver
import time
# Classe POM de la pagina de los resultados de google


class GoogleSearch():
    def __init__(self, driver):
        time.sleep(5)
        self.driver = driver
        self.listaBusqueda = driver.find_elements_by_xpath("//h3[@class=" +
                                              "'LC20lb DKV0Md']")
        self.listaDescripcion = driver.find_elements_by_xpath("//span" +
                                              "[@class='st']")
    # Funcion imprime busqueda imprime los elementos de la lista de resultados

    def imprimeBusqueda(self):
        print("Numero de resultados: ", len(self.listaBusqueda))
        try:
            for pagina in self.listaBusqueda:
                print(pagina.text)
        except:
            print("Elemento no encontrado")
    # Funcion que valida si aparece alguna sugerencia

    def validarSugerencia(self):
        try:
            time.sleep(2)
            sugerenciaText = self.driver.find_element_by_xpath("//a[@class=" +
            "'gL9Hy']")
            sugerenciaText.click()
            print("Sugerencia encontrada y seleccionada")
        except:
            print("Sin suegerencias encontradas")
    # Funcion que valida que el numero de elementos de la lista de resultados
    # sea igual al de la lista de descripciones

    def validarDescripcion(self):
        try:
            # print(len(self.listaDescripcion))
            if len(self.listaBusqueda) == len(self.listaDescripcion):
                print("Todos los elementos tienen descripcion")
                return True
            else:
                print("No todos los elementos tienen descripcion")
        except:
            print("Elemento no encontrado")
        return False