from selenium import webdriver
# Clase SetUp que instancia un webdriver


class SetUp():
    def __init__(self):
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--headless')
        self.driver =  webdriver.Chrome(executable_path=r"C:\\Users\\mvillegas" +
        "\\Documents\\Selenium\\chromedriver (2).exe",
        chrome_options=chrome_options)
        print("Driver creado")

    def  getDriver(self):
        print("Retornando driver....")
        return self.driver
